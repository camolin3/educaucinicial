class EscalasController < ApplicationController

  before_filter  { |c| auth_and_check_privileges([0b10, 0b11]) }

  def new
  end

  def create
  end

  def index
    @escalas = Escala.all
    @subescalas = Subescala.all
    @items = Item.all
    @indicadores = Indicador.all
  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
  end
end
