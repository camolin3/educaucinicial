class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :nombre
      t.integer :puntuacion
      t.text :comentarios
      t.references :subescala

      t.timestamps
    end
  end
end
