class Indicador < ActiveRecord::Base
  attr_accessible :absoluto, :numero, :texto, :valor, :item_id
  belongs_to :item
  
  def self.indicadores_con_respuestas(string_respuestas="")
    result = []
    list = string_respuestas.split(":").delete_if(&:empty?)
    for i in 0..(list.length-1)
      next if i%2 == 1

      # "v" contiene "item.id" e "indicador.numero"
      v = list[i].split("-")
      indicador = Item.find(v[0].to_i).indicadors.where(numero: v[1]).first

      result.append [indicador, list[i+1]]
    end
    result
  end
end
