class Sala < ActiveRecord::Base
  attr_accessible :nombre, :respuestas, :comentarios, :evaluadora_id, :escala_id, :evaluacion_id
  belongs_to :evaluacion
  belongs_to :escala

  def puntajes_subescalas
  	respuestas = Indicador.indicadores_con_respuestas(self.respuestas)
    puntaje_itemes = Item.puntaje_itemes(respuestas)
    puntaje_subescalas = Subescala.puntaje_subescalas(puntaje_itemes)
    puntaje_subescalas
  end
end
