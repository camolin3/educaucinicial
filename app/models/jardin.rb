class Jardin < ActiveRecord::Base
  attr_accessible :nombre, :contacto, :direccion, :email, :telefono
  has_many :evaluacions

  validates :nombre, :presence => true
  validates :contacto, :presence => true
  validates :direccion, :presence => true
  validates :email, :email => true
end
