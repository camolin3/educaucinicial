require 'valid_email/email_validator'
class Usuario < ActiveRecord::Base
  attr_accessible :email, :nombre, :password, :password_confirmation, :permisos
  has_many :evaluacions, :class_name => 'Evaluacion', :foreign_key => 'evaluadora_id'

  attr_accessor :password
  validates :nombre, :presence => true
  validates :email, :presence => true, :uniqueness => true, :email => true
  validates :password, :confirmation => true #password_e_confirmation attr
  validates_length_of :password, :in => 6..25, :on => :create

  before_save :encrypt_password
  after_save :clear_password

  def encrypt_password
    if password.present?
      self.salt = BCrypt::Engine.generate_salt
      self.password_e = BCrypt::Engine.hash_secret(password, salt)
    end
  end

  def clear_password
    self.password = nil
  end

  def self.authenticate(email="", login_password="")
    user = Usuario.find_by_email(email)

    if user && user.match_password(login_password)
      return user
    else
      return false
    end
  end   

  def match_password(login_password="")
    password_e == BCrypt::Engine.hash_secret(login_password, salt)
  end
end
