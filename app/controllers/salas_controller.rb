#<Encoding:UTF-8>
class SalasController < ApplicationController

  # ['Investigador', 0b01], ['Evaluador', 0b10], ['Coordinadora', 0b11]
  before_filter { |c| auth_and_check_privileges([0b10, 0b11]) }

  def index
    @salas = Sala.where(:evaluacion_id => params[:evaluacion_id])
  end

  def show
    @sala = Sala.find(params[:id])
  end

  def new
    @sala = Evaluacion.find(params[:evaluacion_id]).salas.build
    list_of_escalas
  end

  def edit
    @sala = Sala.find(params[:id])
    list_of_escalas
  end

  def create
    @sala = Sala.new(params[:sala])

    if @sala.save
      redirect_to edit_evaluacion_sala_path(@sala.evaluacion_id, @sala.id), notice: 'La nueva sala fue creada.'
    else
      render action: "new"
    end
  end

  def update
    @sala = Sala.find(params[:id])

    if @sala.update_attributes(params[:sala])
      redirect_to evaluacion_sala_path(@sala.id), notice: 'Los cambios se guardaron exitosamente.'
    else
      render action: "edit"
    end
  end

  def destroy
    @sala = Sala.find(params[:id])
    @sala.destroy
    redirect_to edit_evaluacion_path(@sala.evaluacion_id)
  end

  protected
    def list_of_escalas
      @escalas = [['Selecciona una escala', -1]]
      Escala.all.each do |e|
        @escalas.append [e.nombre, e.id]
      end
    end
end
