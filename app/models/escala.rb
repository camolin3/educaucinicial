class Escala < ActiveRecord::Base
  attr_accessible :nombre
  has_many :subescalas
  has_many :salas
end
