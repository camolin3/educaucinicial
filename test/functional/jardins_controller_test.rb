require 'test_helper'

class JardinsControllerTest < ActionController::TestCase
  setup do
    @jardin = jardins(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:jardins)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create jardin" do
    assert_difference('Jardin.count') do
      post :create, jardin: {  }
    end

    assert_redirected_to jardin_path(assigns(:jardin))
  end

  test "should show jardin" do
    get :show, id: @jardin
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @jardin
    assert_response :success
  end

  test "should update jardin" do
    put :update, id: @jardin, jardin: {  }
    assert_redirected_to jardin_path(assigns(:jardin))
  end

  test "should destroy jardin" do
    assert_difference('Jardin.count', -1) do
      delete :destroy, id: @jardin
    end

    assert_redirected_to jardins_path
  end
end
