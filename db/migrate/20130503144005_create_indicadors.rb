class CreateIndicadors < ActiveRecord::Migration
  def change
    create_table :indicadors do |t|
      t.decimal :numero
      t.string :texto
      t.boolean :valor
      t.boolean :absoluto
      t.references :item

      t.timestamps
    end
  end
end
