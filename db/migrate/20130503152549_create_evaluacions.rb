class CreateEvaluacions < ActiveRecord::Migration
  def change
    create_table :evaluacions do |t|
      t.date :fechavisita
      t.references :jardin
      t.belongs_to :evaluadora

      t.timestamps
    end
  end
end
