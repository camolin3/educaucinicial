class Item < ActiveRecord::Base
  attr_accessible :comentarios, :nombre, :puntuacion, :subescala_id
  belongs_to :subescala
  has_many :indicadors

  def self.puntaje_itemes(respuestas=[])
    puntajes = {}
    debo_parar = false
    ultimo_grupo = 1
    grupo_actual = 1
    respuestas_malas = 0

    respuestas.each do |array|
      indicador = array[0]
      respuesta = array[1].to_i
      grupo_actual = indicador.numero.to_i
      
      decimal = ((indicador.numero - grupo_actual) * 10).to_i
      if decimal == 1
        # Reinicio todo
        debo_parar = false
        respuestas_malas = 0
      end
      
      if grupo_actual == 1
        # Las respuestas si=0 hacen que se detenga la evaluación
        if respuesta == 0
          debo_parar = true
        end
        # De todas formas, guardo el puntaje actual en la tabla de hash
        puntajes[indicador.item] = grupo_actual
      else

        # Si se trata de los otros grupos: no=1 hace que se detenga la evaluación
        if respuesta == 1
          debo_parar = true
          respuestas_malas += 1
        end
        # De todas formas, guardo el puntaje actual en la tabla de hash
        puntaje = (respuestas_malas == 0) ? grupo_actual : ((respuestas_malas < 0.5*decimal) ? grupo_actual - 1 : grupo_actual - 2)
        puntajes[indicador.item] =  puntaje
      end
    end
    puntajes
  end

  def self.comentarios_itemes(string_comentario="")
    comentarios = {}
    lista = string_comentario.split('#$%')
    for i in 0..(lista.length-1)
      next if i%2 == 1

      item = Item.find lista[i].split('_')[1]
      comentarios[item] = lista[i+1]
    end
    comentarios
  end
end
