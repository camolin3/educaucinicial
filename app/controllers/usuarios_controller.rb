#<Encoding:UTF-8>
class UsuariosController < ApplicationController
  
  # Sólo pueden acceder los coordinadores
  before_filter { |c| auth_and_check_privileges([0b11]) }

  def new
    @usuario = Usuario.new
  end

  def create
    @usuario = Usuario.new(params[:usuario])
    if @usuario.save
      redirect_to :action => :index
    else
      flash[:notice] = "Form is invalid"
      render "new"
    end
  end

  def index
    @usuarios = Usuario.all
  end

  def show
    @usuario = Usuario.find(params[:id])
  end

  def edit
    @usuario = Usuario.find(params[:id])
  end

  def update
    @usuario = Usuario.find(params[:id])
    if params[:current_password]
      usuario_autorizado = Usuario.authenticate(@usuario_actual.email,params[:current_password])
      if usuario_autorizado
        if @usuario.update_attributes(params[:usuario])
          redirect_to @usuario
        else
          flash[:notice] = "Password Confirmation no coincide con la Nueva Password"
          redirect_to :controller => "sesion", :action => "password"
        end
      else
        flash[:notice] = "Password Actual Invalida"
        redirect_to :controller => "sesion", :action => "password"
      end
    elsif @usuario.update_attributes(params[:usuario])
      redirect_to @usuario
    else
      render action: "edit"
    end
  end

  def destroy
    Usuario.find(params[:id]).destroy
    redirect_to :action => :index
  end
end
