// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

function getCheckedValue(groupName) {
	var radios = document.getElementsByName(groupName);
	for(i = 0; i < radios.length; i++) {
		if(radios[i].checked && !radios[i].disabled)
			return i;
	}
	return null;
}

function getRadioNames() {
	var radios = document.querySelectorAll('input[type="radio"]');
	var names = [];
	for(i = 0; i < radios.length; i++) {
		if (names[names.length-1] != radios[i].name)
			names.push(radios[i].name);
	}
	return names;
}

function encodeAnswers() {
	var names = getRadioNames();
	var answers = "";
	for (var i = 0; i < names.length; i++) {
		var value = getCheckedValue(names[i]);
		if (value != null) {
			answers += ':' + names[i] + ':' + value;
		}
	}
	return answers;
};

function decodeAnswers() {
	var answers = document.getElementById('sala_respuestas').value;
	list = answers.match(/[^:]+/g) // Is like a split for ':'

	for (var i = 0; i < list.length; i+=2) {
		var radios = document.getElementsByName(list[i]);
		radios[list[i+1]].checked = true
	};

	$(':input[type="radio"]').each(function() {
		if($(this).is(":checked")){
			var nameSplited = $(this).attr("name").split("-");
			if(parseInt(nameSplited[1]) == 1){
				if($(this).attr("value") == "s"){
					disableNextRadio(nameSplited[0], nameSplited[1]);
				}
				else if(checkPrevRadio(nameSplited[0], nameSplited[1]) == 1){
					enableNextRadio(nameSplited[0], nameSplited[1]);
				}
			}
			else{
				if($(this).attr("value") == "n"){
					disableNextRadio(nameSplited[0], nameSplited[1]);
				}
				else if(checkPrevRadio(nameSplited[0], nameSplited[1]) == 1){
					enableNextRadio(nameSplited[0], nameSplited[1]);
				}
			}
		}
	});
}

function showAnswers() {
	answers = document.getElementById('showRespuestas').innerHTML;
	list = answers.match(/[^:]+/g) // Is like a split for ':'

	for (var i = 0; i < list.length; i+=2) {
		var fields = document.getElementsByName(list[i]);
		fields[list[i+1]].setAttribute("class", "icon-ok");
	};
}

if (document.forms[0]) {
	document.forms[0].addEventListener('submit', 
		function(event) {
			var container = document.getElementById('sala_respuestas');
			container.value = encodeAnswers();
		}, 
		false
	);
}

if (document.getElementById('sala_button')) {
	document.getElementById('sala_button').onclick = function(){
	    window.btn_clicked = true;
	};
}
if (document.getElementById('sala_button')) {
	window.onbeforeunload = function(){
	    if(!window.btn_clicked){
	        return 'La escala aplicada no se ha guardado, si usted abandona la página perderá los cambios que haya realizado.';
	    }
	};
}

if (window.addEventListener) {
	window.addEventListener('load', decodeAnswers, false); //W3C
	window.addEventListener('load', showAnswers, false); //W3C
}
else {
	window.attachEvent('onload', decodeAnswers); //IE
	window.attachEvent('onload', showAnswers); //IE
}



function getTextNames() {
    var texts = document.querySelectorAll('textarea');
    return texts;
}

function encodeComments() {
    var texts = getTextNames();
    var comments = "";
    for (var i = 0; i < texts.length; i++) {
        if (texts[i].value)
            comments += texts[i].name + '#$%' + texts[i].value + '#$%';
    }
    return comments;
};

function decodeComments() {
    var comments = document.getElementById('sala_comentarios').value;
    list = comments.match(/[^#$%]+/g) // Is like a split for ':'

    for (var i = 0; i < list.length; i+=2) {
        var textarea = document.getElementById(list[i]);
        textarea.value = list[i+1]
    };
}

function showComments() {
    comments = document.getElementById('showComentarios').innerHTML;
    list = comments.match(/[^#$%]+/g) // Is like a split for ':'

    for (var i = 0; i < list.length; i+=2) {
        var textarea = document.getElementById(list[i]);
        textarea.innerHTML = list[i+1];
    };
}

function disableNextRadio(name, number) {
	$(":input[name^='"+ name +"-']").each(function() {
		var nameSplited = $(this).attr("name").split("-");
		if(parseInt(nameSplited[1])>parseInt(number)){
			$(this).prop('disabled', true);
		}
	});
}

function enableNextRadio(name, number) {
	$(":input[name^='"+ name +"-']").each(function() {
		var nameSplited = $(this).attr("name").split("-");
		if(parseInt(nameSplited[1])>parseInt(number)){
			$(this).prop('disabled', false);
		}
	});
}

function checkPrevRadio(name, number) {
	var toRet = 1;
	$(":input[name^='"+ name +"-']").each(function() {
		var nameSplited = $(this).attr("name").split("-");
		if(parseInt(nameSplited[1]) == 1){
			if(parseInt(nameSplited[1]) == parseInt(number) && $(this).attr("value") == "s"){
				if($(this).is(":checked")){
					toRet = 0;
				}
			}
		}else{
			if(parseInt(nameSplited[1]) == parseInt(number) && $(this).attr("value") == "n"){
				if($(this).is(":checked")){
					toRet = 0;
				}
			}
		}
	});
	return toRet;
}

$(document).ready(function() {
	$(':input[type="radio"]').click(function() {
		var nameSplited = $(this).attr("name").split("-");
		if(parseInt(nameSplited[1]) == 1){
			if($(this).attr("value") == "s"){
				disableNextRadio(nameSplited[0], nameSplited[1]);
			}
			else if(checkPrevRadio(nameSplited[0], nameSplited[1]) == 1){
				enableNextRadio(nameSplited[0], nameSplited[1]);
			}
		}
		else{
			if($(this).attr("value") == "n"){
				disableNextRadio(nameSplited[0], nameSplited[1]);
			}
			else if(checkPrevRadio(nameSplited[0], nameSplited[1]) == 1){
				enableNextRadio(nameSplited[0], nameSplited[1]);
			}
		}
	});
});

if (document.forms[0]) {
    document.forms[0].addEventListener('submit',
        function(event) {
            var container = document.getElementById('sala_comentarios');
            container.value = encodeComments();
        },
        false
    );
}

if (window.addEventListener) {
    window.addEventListener('load', decodeComments, false); //W3C
    window.addEventListener('load', showComments, false); //W3C
}
else {
    window.attachEvent('onload', decodeComments); //IE
    window.attachEvent('onload', showComments); //IE
}