class CreateEscalas < ActiveRecord::Migration
  def change
    create_table :escalas do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
