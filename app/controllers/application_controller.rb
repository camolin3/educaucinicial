class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :check_privileges

  def check_privileges(kind_of_users)
    return kind_of_users.include?(@usuario_actual.permisos.to_i())
  end

  protected
    def auth_and_check_privileges(kind_of_users)
      # Revisa que el usuario esté logueado y tenga los permisos
      return authenticate_user && check_privileges(kind_of_users)
    end

    def authenticate_user
      unless session[:usuario_id]
        redirect_to(:controller => 'sesion', :action => 'login')
        return false
      else
        # Setea como @usuario_actual al usuario logueado
        @usuario_actual = Usuario.find session[:usuario_id]
        return true
      end
    end

    def save_login_state
      # No permite acceder a la página si el usuario ya está logueado
      return !session[:usuario_id]
    end
    
end
