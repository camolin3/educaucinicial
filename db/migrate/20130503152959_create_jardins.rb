class CreateJardins < ActiveRecord::Migration
  def change
    create_table :jardins do |t|
      t.string :nombre
      t.string :direccion
      t.string :contacto
      t.string :telefono
      t.string :email

      t.timestamps
    end
  end
end
