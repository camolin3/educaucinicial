#<Encoding:UTF-8>
class JardinsController < ApplicationController

  before_filter :authenticate_user

  def index
    @jardins = Jardin.all
  end

  def show
    @jardin = Jardin.find(params[:id])
  end

  def new
    @jardin = Jardin.new
  end

  def edit
    @jardin = Jardin.find(params[:id])
  end

  def create
    @jardin = Jardin.new(params[:jardin])

    if @jardin.save
      redirect_to @jardin, notice: 'El jardín fue registrado.'
    else
      render action: "new"
    end
  end

  def update
    @jardin = Jardin.find(params[:id])
    if @jardin.update_attributes(params[:jardin])
      redirect_to @jardin, notice: 'El jardín fue actualizado.'
    else
      render action: "edit"
    end
  end

  def destroy
    Jardin.find(params[:id]).destroy
    redirect_to jardins_url
  end
end
