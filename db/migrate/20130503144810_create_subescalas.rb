class CreateSubescalas < ActiveRecord::Migration
  def change
    create_table :subescalas do |t|
      t.string :nombre
      t.references :escala

      t.timestamps
    end
  end
end
