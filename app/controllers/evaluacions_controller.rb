#<Encoding:UTF-8>
class EvaluacionsController < ApplicationController
  require 'spreadsheet'
  Spreadsheet.client_encoding = 'UTF-8'
  require 'gchart'
  require 'rtf'
  include RTF

  # ['Investigador', 0b01], ['Evaluador', 0b10], ['Coordinadora', 0b11]
  before_filter { |c| auth_and_check_privileges([0b10, 0b11]) }

  def index
    @intro = case @usuario_actual.permisos.to_i()
      when 0b10 then 'Todas las evaluaciones realizadas por ti'
      when 0b11 then 'Todas las evaluaciones'
    end

    @evaluaciones = case @usuario_actual.permisos.to_i()
      when 0b10
        then @usuario_actual.evaluacions
      when 0b11
        then Evaluacion.all
    end

  end

  def show
    @evaluacion = Evaluacion.find(params[:id])
  end


  def to_excel
    @evaluacion = Evaluacion.find(params[:id])

    book = Spreadsheet::Workbook.new
    header = Spreadsheet::Format.new :weight => :bold,
                                     :pattern_fg_color => :yellow,
                                     :pattern => 1

    @evaluacion.salas.each do |sala|
      sheet1 = book.create_worksheet :name => "#{sala.nombre}"

      sheet1.row(0).push "Jardin evaluado"
      sheet1.row(0).push @evaluacion.jardin.nombre
      sheet1.row(1).push "Fecha de la visita"
      sheet1.row(1).push @evaluacion.fechavisita.to_s
      sheet1.row(2).push "Evaluadora"
      sheet1.row(2).push @evaluacion.evaluadora.nombre
      sheet1.row(3).push "Sala"
      sheet1.row(3).push sala.nombre
      sheet1.row(4).push "Escala utilizada"

      if sala.escala_id == 1
        escalaName = "ITERS-R"
        escala = Escala.find_by_nombre("ITERS-R")
      elsif
      escalaName = "ECERS-R"
        escala = Escala.find_by_nombre("ECERS-R")
      end

      sheet1.row(4).push escalaName
      sheet1.row(6).concat %w{Nivel Calificacion Comentarios}
      sheet1.row(6).default_format = header
      i = 7
      escala.subescalas.each do |sub|
        sheet1.row(i).push sub.nombre
        sheet1.row(i).default_format = header
        i=i+1
        sub.items.each do |it|
          sheet1.row(i).push it.nombre

          i=i+1
        end
      end
      #respuestas = Indicador.indicadores_con_respuestas(sala.respuestas)
      #puntaje_itemes = Item.puntaje_itemes(respuestas)
      #sheet1.row(0).push puntaje_itemes.first.find(2).to_s
      #sheet1.row(1).push Item.puntaje_itemes(sala.respuestas).to_s
      #sheet1.row(0).push sala.puntajes_subescalas.to_s
    end

    spreadsheet = StringIO.new
    book.write spreadsheet
    send_data spreadsheet.string, :filename => "evaluacion.xls", :type =>  "application/vnd.ms-excel"

  end


  def informe
    @evaluacion = Evaluacion.find(params[:id])

    # Create required styles.
    styles = {}
    # Header for first page
    styles['CS_HEADER']               = CharacterStyle.new
    styles['CS_HEADER'].bold          = true
    styles['CS_HEADER'].font_size     = 36
    styles['PS_HEADER']               = ParagraphStyle.new
    styles['PS_HEADER'].justification = ParagraphStyle::CENTRE_JUSTIFY
    # H1
    styles['CS_H1']               = CharacterStyle.new
    styles['CS_H1'].bold          = true
    styles['CS_H1'].capitalize    = true
    styles['PS_H1']               = ParagraphStyle.new
    styles['PS_H1'].space_after   = 600 # doesn't work
    styles['PS_H1'].space_before  = 900
    # Normal
    styles['PS_NORMAL']               = ParagraphStyle.new
    styles['PS_NORMAL'].justification = ParagraphStyle::FULL_JUSTIFY
    styles['PS_NORMAL'].space_after   = 150
    styles['PS_NORMAL'].space_before  = 150
    # Footer
    styles['CS_FOOTER']           = CharacterStyle.new
    styles['CS_FOOTER'].font_size = 20

    document = Document.new(Font.new(Font::ROMAN, 'Arial'))

    # Portada
    document.paragraph(styles['PS_HEADER']) do |p|
      p.apply(styles['CS_HEADER']) do |s|
        10.times{ s.line_break }
        s << "INFORME DE RESULTADOS"
        2.times{ s.line_break }
        s << "Escalas de Calificación del ambiente Educativo"
        s.line_break
        s << "(ITERS-R/ECERS-R)"
        2.times{ s.line_break }
        s << @evaluacion.jardin.nombre
        10.times{ s.line_break }
        s << "EducaUC Inicial"
        s.line_break
        mes = case @evaluacion.fechavisita.month
        when 1  ; "Enero"
        when 2  ; "Febrero"
        when 3  ; "Marzo"
        when 4  ; "Abril"
        when 5  ; "Mayo"
        when 6  ; "Junio"
        when 7  ; "Julio"
        when 8  ; "Agosto"
        when 9  ; "Septiembre"
        when 10 ; "Octubre"
        when 11 ; "Noviembre"
        when 12 ; "Diciembre"
        end
        s << "#{mes} #{@evaluacion.fechavisita.year}"
      end
    end
    document.page_break

    # Presentación
    document.paragraph(styles['PS_NORMAL']) << ""
    document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
      q << "Presentación"
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p.italic << "EducaUC Inicial "
      p << "como parte de su gestión, ha adoptado el uso de las Escalas de Calificación del Ambiente de la Infancia Temprana ITERS-R y ECERS-R, para establecer la calidad de los ambientes educativos de centros educativos de primera infancia. "
      p << "Estas Escalas fueron desarrolladas por investigadores del Centro de Desarrollo Infantil Frank Porter Graham, de la Universidad de Carolina del Norte de Estados Unidos y han sido parte importante de las investigaciones de primera infancia tanto en el extranjero como en nuestro país."
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "El presente informe tiene como objetivo conocer y analizar los resultados obtenidos en la aplicación de ambos instrumentos evaluativos en el "
      p.bold << @evaluacion.jardin.nombre
      p << " de la comuna de #{@evaluacion.jardin.direccion.split(", ").last}. "
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "Su estructura consta de tres apartados; en  el primero de ellos se da a conocer una breve descripción de las características de los instrumentos aplicados, acompañada del resumen de los contenidos que aborda cada Escala."
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "Posteriormente, en el segundo punto, se entregan algunas consideraciones respecto a los procedimientos de aplicación y los resguardos que se  deben  considerar frente a los resultados de las Escalas de Calificación en términos generales."
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "En tercer lugar, se presentan los resultados obtenidos por el " + @evaluacion.jardin.nombre + "; un breve resumen  de los resultados generales del Centro educativo y el detalle de cada sala evaluada."
    end
    document.page_break

    # I. Antecendentes de la escala
    document.paragraph(styles['PS_NORMAL']) << ""
    document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
      q << "I. Antecendentes de la escala"
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "A continuación se detalla información respecto a las características principales de las Escalas aplicadas"
    end
    table = document.table(2, 3, 2400, 2800, 2800)
    table.cell_margin = 400
    table.border_width = 1
    table[0][0].bold << "Escalas"
    table[0][1].bold << "ITERS-R"
    table[0][2].bold << "ECERS-R"
    table[1][0].bold << "Rango de edad "
    table[1][1]      << "Desde los primeros meses hasta los 2 años y medio"
    table[1][2]      << "Desde los 2 años y medio hasta los 5 años"
    table = document.table(2, 2, 2400, 5600)
    table.cell_margin = 400
    table.border_width = 1
    table[0][0].bold << "Sub Escalas"
    table[0][1] << "• Espacio y Muebles"
    table[0][1].line_break
    table[0][1] << "• Rutinas de Cuidado Personal"
    table[0][1].line_break
    table[0][1] << "• Escuchar y Hablar / Lenguaje-Razonamiento"
    table[0][1].line_break
    table[0][1] << "• Actividades"
    table[0][1].line_break
    table[0][1] << "• Interacción"
    table[0][1].line_break
    table[0][1] << "• Estructura del programa"
    table[0][1].line_break
    table[0][1] << "• Padres y Personal"
    table[1][0].bold << "Antecendentes"
    table[1][1] << "Evalúan la calidad del proceso y cuantifican la observación de lo que ocurre en una sala de clases."
    2.times { table[1][1].line_break }
    table[1][1] << "Cuentan con respaldo científico: estudios de validación, confiabilidad y coherencia interna."
    2.times { table[1][1].line_break }
    table[1][1] << "Las fuentes principales para su creación tienen su base en evidencias empíricas: pruebas de investigación en el campo de la salud, del desarrollo y de la educación."
    2.times { table[1][1].line_break }
    table[1][1] << "Han sido utilizadas en importantes proyectos de investigación, lo que ha evidenciado su confiabilidad en a cuanto uso y validez (EEUU, Canadá, Alemania, Italia, Rusia, Inglaterra, Brasil, Hong Kong, Corea, entre otros)."
    document.page_break

    # Resumen de las Sub-escalas y los ítems para tipo de Escala (ITERS-R/ECERS-R)
    document.paragraph(styles['PS_NORMAL']) << ""
    document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
      q << "Resumen de las Sub-escalas y los ítems para tipo de Escala (ITERS-R/ECERS-R)"
    end
    escalas = ["ITERS-R", "ECERS-R"]
    table = document.table(2, 2, 4000, 4000)
    table.cell_margin = 400
    table.border_width = 1
    table[0][0].bold << escalas[0]
    table[0][1].bold << escalas[1]
    
    for c in 0..1
      count = 1
      Subescala.where(escala_id: Escala.where(nombre: escalas[c])).each do |s|
        table[1][c].bold << s.nombre
        table[1][c].line_break
        Item.where(subescala_id: s).each do |i|
          table[1][c] << "#{count}. #{i.nombre}"
          table[1][c].line_break
          count += 1
        end
      end
    end
    document.page_break

    # II. CONSIDERACIONES ACERCA DE LA APLICACIÓN Y RESULTADO DE LAS ESCALAS
    document.paragraph(styles['PS_NORMAL']) << ""
    document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
      q << "II. Consideraciones acerca de la aplicación y resultado de las escalas"
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "Los estándares de calidad que dan origen las Escalas de Calificación del Ambiente Educativo (ITERS-R/ECER-S) reflejan un alto grado de exigencia, dando cuenta de lo que se estima como óptimo a lograr en un Centro Educativo a nivel internacional. "
      p << "En este contexto, es válido tener presente que los resultados obtenidos podrán estimarse como bajos en relación a resultados obtenidos en instrumentos nacionales."
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "Los rangos de calificación de las Escalas dan cuenta de la pertinencia de las prácticas del Centro educativo para el óptimo desarrollo y aprendizaje de los niños y niñas. Se organizan del siguiente modo:"
    end
    document.paragraph.bold << "<< Falta una imagen aquí >>"
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "Los indicadores que aparecen en las tablas de resultados, con numeración 1 (1.1; 1.2; 1,3; etc.) están expresados en redacción negativa en la versión original de las Escalas y corresponden a aquellos aspectos que nunca deberían presentarse en un Centro."
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "Es necesario tener presente que los resultados obtenidos corresponden a lo revelado al momento de la observación realizada en cada aula, sumado a algunos aspectos recogidos de manera complementaria con el adulto a cargo del nivel."
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "La información entregada en el presente informe da cuenta de un promedio de las prácticas desarrolladas al interior del aula por todo el personal pedagógico, es decir de las educadoras y personal técnico presentes durante la observación. "
      p << "En ningún caso la evaluación se orienta exclusivamente al desempeño de la Educadora a cargo del nivel."
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      if true
        p.italic << "No se incorpora la Sub Escala Padres y Personal, por no haber sido aplicada en el Centro Educativo. "
        p.italic << "Los ítems que aparecen como NA son aquellos no aplicados debido a las características del Centro o de la Sala."
        p.line_break
      end
    end
    document.page_break

    # III. RESULTADOS GENERALES CENTRO
    document.paragraph(styles['PS_NORMAL']) << ""
    document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
      q << "III. Resultados generales centro"
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "DATOS DE LA EVALUACIÓN"
    end
    table = document.table(5, 2, 3000, 6000)
    table.cell_margin = 400
    table.border_width = 1
    table[0][0] << "Centro educativo"
    table[0][1] << @evaluacion.jardin.nombre
    table[1][0] << "Directora"
    table[1][1] << @evaluacion.jardin.contacto
    table[2][0] << "Fecha"
    table[2][1] << @evaluacion.fechavisita.to_s
    table[3][0] << "Niveles evaluados"
    table[3][1].italic << "<<Esto está pendiente>>"
    table[4][0] << "Evaluadoras"
    table[4][1] << @evaluacion.evaluadora.nombre

    document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
      q << "PUNTUACIÓN GENERAL DEL CENTRO"
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "A continuación se presenta el puntaje promedio obtenido por el Centro en su conjunto para cada Sub Escala."
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << 'Los ítems con puntajes iguales o superiores a 3 son considerados por las Escalas de Calificación del Ambiente Educativo como prácticas apropiadas al desarrollo y aprendizaje, por lo que se ubican en un rango de calidad que va desde "Mínimo" (3 puntos) a "Excelente" (7 puntos). '
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "Estos Ítems son considerados como los aspectos más fuertes en el Centro, ya que promueven y apoyan el desarrollo y aprendizaje positivo del niño/a."
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "Los ítems con puntajes inferiores a 3 en las Escalas de Calificación del Ambiente Educativo reflejan prácticas no apropiadas o insuficientes para el desarrollo y aprendizaje de los párvulos."
    end
    document.page_break

    subescalas = Escala.first.subescalas # Las subescalas de ambas escalas son idénticas en sus nombres
    table = document.table(subescalas.length+2, 2, 4000, 4000)
    table.cell_margin = 400
    table.border_width = 1
    table[0][0].bold << "Subescala"
    table[0][1].bold << "Puntaje"
    r = 1
    # Obtengo los puntajes de las subescalas de cada una de las salas
    puntajes_subescalas = {}
    subescalas_salas = {}
    @evaluacion.salas.each do |sala|
      subescalas_sala = sala.puntajes_subescalas
      subescalas_salas[sala.nombre] = subescalas_sala

      subescalas.each do |se|
        if puntajes_subescalas.has_key?(se.nombre)
          suma = puntajes_subescalas[se.nombre][0].to_f + subescalas_sala[se.nombre].to_f
          cantidad = puntajes_subescalas[se.nombre][1].to_i + 1
        else
          if subescalas_sala.has_key?(se.nombre)
            suma = subescalas_sala[se.nombre]
            cantidad = 1
          else
            suma = 0.0
            cantidad = 0
          end
        end
        puntajes_subescalas[se.nombre] = [suma, cantidad]
      end
    end

    total_score = 0
    total_subescalas = 0
    data_centro = []
    legend = []
    subescalas.each do |subescala|
      arr = puntajes_subescalas[subescala.nombre]

      if arr[0] == 0.0
        puntaje = "NA"
        data_centro.append([0.0])
      else
        puntaje = (arr[1] != 0) ? arr[0]/arr[1] : 0.0
        data_centro.append([puntaje])
        total_score += puntaje
        total_subescalas += 1
      end
      legend.append(subescala.nombre)
      table[r][0].bold << subescala.nombre
      table[r][1] << ((puntaje.kind_of? Float) ? puntaje.round(1).to_s : puntaje)
      r += 1
    end
    table[r][0].bold << "PUNTUACIÓN TOTAL"
    table[r][1].bold << ((total_subescalas==0) ? 0 : (total_score/total_subescalas)).round(1).to_s

    document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
      q << "GRÁFICO GENERAL "
      q.underline << "DEL CENTRO"
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "A continuación se presenta un gráfico resumen con los puntajes obtenidos por el "
      p.bold << "Centro en cada Sub Escala."
    end
    grafico_promedio_centro = Gchart.new(
      :type => 'bar',
      :size => '550x280',
      :bar_colors => ['76A4FB', 'FC68C5', 'D2FE69', 'FFCA69', '2652A3', 'A42274', '7EA522', 'A67822'],
      :bg => 'EFEFEF',
      :title => 'GRÁFICO PROMEDIO CENTRO', 
      :stacked => false,
      :data => data_centro, 
      :legend => legend,
      :legend_position => 'vertical',
      :axis_with_labels => [['y']],
      :min_value => 0,
      :max_value => 7,
      :bar_width_and_spacing => '25,25',
      :filename => Rails.root.join("public", "grafico_promedio_centro_#{@evaluacion.id}.png").to_s)
    grafico_promedio_centro.file
    document.paragraph.image(grafico_promedio_centro.filename)
    document.page_break

    document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
      q << "GRÁFICO COMPARATIVO "
      q.underline << "SALAS"
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p << "A continuación se presenta un gráfico resumen que da cuenta de las diferencias entre las salas evaluadas para cada "
      p.bold << "Sub Escala."
    end
    subescala_promedio = []
    data_centro.each do |arr| subescala_promedio.append arr.first end
    
    data_salas = []
    legend = []
    # Agrego los datos del centro
    data_salas.append(subescala_promedio)
    legend.append("Centro")
    subescalas_salas.each do |nombre_sala, se_sala|
      se = []
      subescalas.each do |subescala|
        if se_sala.has_key?(subescala.nombre)
          se.append(se_sala[subescala.nombre])
        else
          se.append(0.0)
        end
      end
      data_salas.append(se)
      legend.append(nombre_sala)
    end
    grafico_comparativo_salas = Gchart.new(
      :type => 'bar',
      :size => '600x280',
      :bar_colors => ['76A4FB', 'FC68C5', 'D2FE69', 'FFCA69', '2652A3', 'A42274', '7EA522', 'A67822'],
      :bg => 'EFEFEF',
      :title => 'GRÁFICO COMPARATIVO SALAS', 
      :stacked => false,
      :data => data_salas, 
      :legend => legend,
      :legend_position => 'vertical',
      :axis_with_labels => [['y']],
      :min_value => 0,
      :max_value => 7,
      :bar_width_and_spacing => '10,10,17',
      :filename => Rails.root.join("public", "grafico_comparativo_salas_#{@evaluacion.id}.png").to_s)
    grafico_comparativo_salas.file
    document.paragraph.image(grafico_comparativo_salas.filename)

    document.page_break

    # IV. TABLAS RESUMEN DE RESULTADOS
    document.paragraph(styles['PS_NORMAL']) << ""
    document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
      q << "IV. TABLAS RESUMEN DE RESULTADOS"
    end
    document.paragraph do |p|
      p << "Tabla Comparativa Resultados por Salas - Escala ITERS-R"
      p.line_break
      p.italic << "<<Falta una tabla con resultados>>"
    end
    document.paragraph do |p|
      p << "Tabla Comparativa Resultados por Salas - Escala ECERS-R"
      p.line_break
      p.italic << "<<Falta una tabla con resultados>>"
    end
    document.page_break

    s = 1
    
    document.paragraph(styles['PS_NORMAL']) << ""
    document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
      q << "V. DETALLE RESULTADOS POR SALA"
    end
    @evaluacion.salas.each do |sala|
      document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
        q << "#{s}. Informe de resultados "
        q.underline << "#{sala.nombre}"
      end
      s+=1

      document.paragraph(styles['PS_NORMAL']) do |p|
        p << "La siguiente descripción corresponde al resultado obtenido por la "
        p.bold << sala.nombre
        p.line_break
      end

      s=1
      grafico_sala_i = Gchart.new(
        :type => 'bar',
        :size => '550x280',
        :bar_colors => ['76A4FB', 'FC68C5', 'D2FE69', 'FFCA69', '2652A3', 'A42274', '7EA522', 'A67822'],
        :bg => 'EFEFEF',
        :title => sala.nombre, 
        :stacked => false,
        :data => data_salas[s], 
        :legend => sala.nombre,
        :legend_position => 'vertical',
        :axis_with_labels => [['y']],
        :min_value => 0,
        :max_value => 7,
        :bar_width_and_spacing => '25,25,25',
        :filename => Rails.root.join("public", "grafico_salas_#{sala.id}_#{@evaluacion.id}.png").to_s)
      grafico_sala_i.file
      document.paragraph.image(grafico_sala_i.filename)
      s+=1

      respuestas = Indicador.indicadores_con_respuestas(sala.respuestas)
      puntaje_itemes = Item.puntaje_itemes(respuestas)

      # FORTALEZAS: ITEMES CON RESULTADOS IGUALES O SUPERIORES A 3
      document.paragraph(styles['PS_NORMAL']) << ""
      document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
        q << "FORTALEZAS: ITEMES CON RESULTADOS IGUALES O SUPERIORES A 3"
      end
      document.paragraph(styles['PS_NORMAL']) do |p|
        p << "En esta sección se describen los ítems con puntajes iguales o superiores a 3. "
        p << "Las puntuaciones en este rango son consideradas por las Escalas de Calificación del Ambiente Educativo como prácticas apropiadas al desarrollo y aprendizaje, por lo que se ubican en un rango de calidad que va desde “Mínimo” (3 puntos) a “Excelente” (7 puntos). "
      end
      document.paragraph(styles['PS_NORMAL']) do |p|
        p << "Estos Ítems son considerados como los aspectos más fuertes en esta Sala, ya que promueven y apoyan el desarrollo y aprendizaje positivo del niño/a."
      end
    
      fortalezas = puntaje_itemes.select do |item,puntaje| puntaje>=3 end
      table = document.table(1+fortalezas.length, 4, 1500, 2500, 1500, 3000)
      table.border_width = 1
      table[0][0].bold << "Sub Escala"
      table[0][1].bold << "Ítem"
      table[0][2].bold << "Puntuación"
      table[0][3].bold << "Indicador"
      r = 1
      fortalezas.each do |item,puntaje|
        table[r][0] << item.subescala.nombre
        table[r][1] << item.nombre
        table[r][2] << puntaje.to_s
        indicadores_del_item = respuestas.select do |array| array[0].item == item end
        ultimo_grupo = indicadores_del_item.last[0].numero.to_i
        indicadores_del_item.reverse_each do |array|
          indicador = array[0]
          if indicador.numero.to_i == ultimo_grupo
            # Si respondió que si=0
            if array[1].to_i == 0
              table[r][3] << "#{indicador.numero}. #{indicador.texto}"
              table[r][3].line_break
            end
          else
            break
          end
        end
        r += 1
      end
      document.page_break

      # ÁREAS DE CRECIMIENTO POTENCIAL: ITEMS CON PUNTAJES INFERIORES A 3
      document.paragraph(styles['PS_NORMAL']) << ""
      document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
        q << "ÁREAS DE CRECIMIENTO POTENCIAL: ITEMS CON PUNTAJES INFERIORES A 3"
      end
      document.paragraph(styles['PS_NORMAL']) do |p|
        p << "Los ítems con puntajes inferiores a 3 en las Escalas de Calificación del Ambiente Educativo reflejan prácticas no apropiadas para el desarrollo y aprendizaje del  niño/a."
      end
      document.paragraph(styles['PS_NORMAL']) do |p|
        p << 'La sección "áreas de crecimiento potencial" proporciona la justificación de los indicadores logrados o no logrados. '
        p << "Este detalle puede ayudar a entender cómo el evaluador llegó a la puntuación de cada ítem de esta sección."
      end

      debilidades = puntaje_itemes.select do |item,puntaje| puntaje<3 end
      table = document.table(1+debilidades.length, 5, 1500, 1500, 1500, 2250, 2250)
      table.border_width = 1
      table[0][0].bold << "Sub Escala"
      table[0][1].bold << "Ítem"
      table[0][2].bold << "Puntuación"
      table[0][3].bold << "Indicador"
      table[0][4].bold << "Justificación"
      r = 1
      comentarios = Item.comentarios_itemes(sala.comentarios)
      debilidades.each do |item,puntaje|
        table[r][0] << item.subescala.nombre
        table[r][1] << item.nombre
        table[r][2] << puntaje.to_s
        indicadores_del_item = respuestas.select do |array| array[0].item == item end
        indicadores_del_item.each do |array|
          indicador = array[0]
          # Si respondió que no=1 y es de un grupo > 1, ó si respondió que si=0 en el grupo 1
          if (array[1].to_i == 1 && indicador.numero.to_i > 1) || (array[1].to_i == 0 && indicador.numero.to_i == 1)
            table[r][3] << "#{indicador.numero}. #{indicador.texto}"
            table[r][3].line_break
          end
        end
        table[r][4] << comentarios[item]
        r += 1
      end
      document.page_break
    end

    # VI. CONCLUSIONES
    document.paragraph(styles['PS_NORMAL']) << ""
    document.paragraph(styles['PS_H1']).apply(styles['CS_H1']) do |q|
      q << "VI. Conclusiones"
    end
    document.paragraph(styles['PS_NORMAL']) do |p|
      p.italic << "<<<Falta la conclusión>>>"
    end

    # Agrego un footer
    footer = FooterNode.new(document)
    footer.paragraph(styles['CS_FOOTER']) << "La Niña 3086, Las Condes, Santiago - Chile  -  Tel.: (56-2) 2207 1010  -  www.educaucinicial.cl"
    document.footer = footer

    # Agrego un header
    header = HeaderNode.new(document)
    header.paragraph.image(Rails.root.join("app", "assets", "images", "educauc.png").to_s)
    document.header = header

    send_data document.to_rtf, :filename=>"informe_#{params[:id]}.rtf", :type=>"text/richtext"

    File.delete(grafico_promedio_centro.filename)
    File.delete(grafico_comparativo_salas.filename)
  end

  def new
    @evaluacion = @usuario_actual.evaluacions.new
    list_of_jardines
  end

  def edit
    @evaluacion = Evaluacion.find(params[:id])
    list_of_jardines
  end

  def create
    @evaluacion = @usuario_actual.evaluacions.new(params[:evaluacion])

    if @evaluacion.save
      redirect_to :action => "edit", :id => @evaluacion.id
    else
      render action: "new"
    end
  end

  def update
    @evaluacion = Evaluacion.find(params[:id])

    if @evaluacion.update_attributes(params[:evaluacion])
      redirect_to :action => "index"
    else
      render action: "edit"
    end
  end

  def destroy
    Evaluacion.find(params[:id]).destroy
    redirect_to :action => :index
  end

  protected
    def list_of_jardines
      @jardines = [['Selecciona un jardín', -1]]
      Jardin.all.each do |j|
        @jardines.append [j.nombre, j.id]
      end
    end
end
