class Evaluacion < ActiveRecord::Base
  attr_accessible :fechavisita, :jardin_id
  belongs_to :jardin
  belongs_to :evaluadora, :class_name => 'Usuario'
  has_many :salas
end
