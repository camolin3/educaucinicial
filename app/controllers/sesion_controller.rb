class SesionController < ApplicationController

  before_filter :authenticate_user, :only => [:home, :profile, :password]
  before_filter :save_login_state, :only => [:login, :login_attempt]

  def login
  end

  def login_attempt
    usuario_autorizado = Usuario.authenticate(params[:email],params[:login_password])
    if usuario_autorizado
      # Hago login del usuario
      session[:usuario_id] = usuario_autorizado.id
      flash[:notice] = "Wow Welcome again, you logged in as #{usuario_autorizado.nombre}"
      redirect_to(:controller => 'evaluacions', :action => 'index')
    else
      flash[:notice] = "Invalid Username or Password"
      flash[:color]= "invalid"
      render "login"
    end
  end

  def logout
    session[:usuario_id] = nil
    redirect_to :action => 'login', :notice => "Logged out!"
  end

  def home
  end

  def profile
  end

  def password
  end
end
