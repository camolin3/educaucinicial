class CreateSalas < ActiveRecord::Migration
  def change
    create_table :salas do |t|
      t.string :nombre
      t.text :respuestas
      t.text :comentarios
      t.references :evaluacion
      t.references :escala

      t.timestamps
    end
  end
end
