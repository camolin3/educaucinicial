class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :nombre
      t.string :email
      t.binary :permisos
      t.string :password_e
      t.string :salt

      t.timestamps
    end
  end
end
