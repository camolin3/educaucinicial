# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130624201550) do

  create_table "escalas", :force => true do |t|
    t.string   "nombre"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "estadisticas", :force => true do |t|
    t.string   "descripcion"
    t.datetime "fecha"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "evaluacions", :force => true do |t|
    t.date     "fechavisita"
    t.integer  "jardin_id"
    t.integer  "evaluadora_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "indicadors", :force => true do |t|
    t.decimal  "numero"
    t.string   "texto"
    t.boolean  "valor"
    t.boolean  "absoluto"
    t.integer  "item_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "items", :force => true do |t|
    t.string   "nombre"
    t.integer  "puntuacion"
    t.text     "comentarios"
    t.integer  "subescala_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "jardins", :force => true do |t|
    t.string   "nombre"
    t.string   "direccion"
    t.string   "contacto"
    t.string   "telefono"
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "salas", :force => true do |t|
    t.string   "nombre"
    t.text     "respuestas"
    t.text     "comentarios"
    t.integer  "evaluacion_id"
    t.integer  "escala_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "subescalas", :force => true do |t|
    t.string   "nombre"
    t.integer  "escala_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "usuarios", :force => true do |t|
    t.string   "nombre"
    t.string   "email"
    t.binary   "permisos"
    t.string   "password_e"
    t.string   "salt"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
