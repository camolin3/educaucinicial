class CreateEstadisticas < ActiveRecord::Migration
  def change
    create_table :estadisticas do |t|
      t.string :descripcion
      t.datetime :fecha

      t.timestamps
    end
  end
end
