class Subescala < ActiveRecord::Base
  attr_accessible :nombre, :escala_id
  belongs_to :escala
  has_many :items

  def self.puntaje_subescalas(respuestas)
    puntajes = {}
    ready = false

    respuestas.each do |item,puntaje|
      subescala = item.subescala
      if puntajes.has_key? subescala
        original = puntajes[subescala]
        puntajes[subescala] = [ original[0]+puntaje, original[1]+1 ]
      else
        puntajes[subescala] = [ puntaje, 1 ]
      end
    end

    puntajes_promedio = {}
    puntajes.each do |subescala, arr|
      puntajes_promedio[subescala.nombre] = arr[0]/arr[1].to_f
    end
    puntajes_promedio
  end
end
